package com.payhere.account.exception;

import com.payhere.account.domain.Response.Response;
import com.payhere.account.exception.customException.AccountException;
import com.payhere.account.exception.customException.RecordException;
import com.payhere.account.exception.customException.UserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ExceptionManager {
    /**UserException**/
    @ExceptionHandler(UserException.class)
    public ResponseEntity<?> userAppExceptionHandler(UserException e) {
        ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(), e.getMessage());
        return ResponseEntity.status(e.getErrorCode().getStatus())
                .body(Response.error("ERROR", errorResponse));

    }

    /**AccountException**/
    @ExceptionHandler(AccountException.class)
    public ResponseEntity<?> accountExceptionHandler(AccountException e) {
        ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(), e.getMessage());
        return ResponseEntity.status(e.getErrorCode().getStatus())
                .body(Response.error("ERROR", errorResponse));

    }

    /**RecordException**/
    @ExceptionHandler(RecordException.class)
    public ResponseEntity<?> recordExceptionHandler(RecordException e) {
        ErrorResponse errorResponse = new ErrorResponse(e.getErrorCode(), e.getMessage());
        return ResponseEntity.status(e.getErrorCode().getStatus())
                .body(Response.error("ERROR", errorResponse));

    }





}